pub fn main() {
    let b = Box::new(5);
    println!("b = {}", b);
    let optional = None;
    check_optional(optional);

    let optional = Some(Box::new(9000));
    check_optional(optional);

    fn check_optional(optional: Option<Box<i32>>) {
        match optional {
            Some(p) => println!("has value {}", p),
            None => println!("has no value"),
        }
    }

    #[derive(Debug)]
    enum Version {
        Version1,
        Version2,
    }

    fn parse_version(header: &[u8]) -> Result<Version, &str> {
        match header.get(0) {
            None => Err("invalid header length"),
            Some(&1) => Ok(Version::Version1),
            Some(&2) => Ok(Version::Version2),
            Some(_) => Err("invalid version"),
        }
    }

    let version = parse_version(&[1, 2, 3, 4]);
    match version {
        Ok(v) => println!("working with version: {:?}", v),
        Err(e) => println!("error parsing header: {:?}", e),
    }
}
