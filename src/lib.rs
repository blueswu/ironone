pub mod sumod {
    pub fn somea<'a>() -> &'a str {
        "helo"
    }
    pub fn someb(a: i32) -> i32 {
        a + 99
    }
}

pub use sumod::{somea, someb};
