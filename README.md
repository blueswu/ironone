# ironone
rust rest template with iron framework

- RUST_LOG=debug run
- cargo run --example somes
- cargo run --bin somes
- cargo build --release
- cargo fmt 
- cargo vendor 

# cross build

$HOME/.cargo/config

```toml
[target.arm-linux-androideabi]
linker = "bin/arm-linux-androideabi-gcc"
```

```

RUSTFLAGS='-C target-feature=+crt-static' cargo build --release --target x86_64-unknown-linux-musl # static
cargo build --target=arm-linux-androideabi
```

